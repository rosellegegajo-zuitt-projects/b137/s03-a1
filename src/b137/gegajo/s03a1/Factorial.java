package b137.gegajo.s03a1;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args){
        System.out.println("Factorial");

        Scanner appScanner = new Scanner(System.in);
        System.out.println("Enter a Number: ");
        int number = appScanner.nextInt();
        int fact = 1;
        for( int i = 1; i <= number; i++){
            fact = fact * i;
        }
        System.out.println("Factorial of the number: " + fact);
    }
}
